FROM php:8.2.2-fpm-buster

RUN mkdir /app

# Copy composer.lock and composer.json
# COPY composer.lock composer.json /app/

COPY docker/8.2/php/app.ini /usr/local/etc/php/conf.d/app.ini
COPY docker/8.2/php/php.ini /usr/local/etc/php/php.ini
COPY docker/8.2/php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY docker/8.2/php/www.conf /usr/local/etc/php-fpm.d/www.conf

# Set working directory
WORKDIR /app

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    libonig-dev \
    zlib1g-dev \
    libzip-dev \
    libpq-dev \
    wget

RUN apt remove nodejs nodejs-legacy npm
RUN rm -rf /usr/local/bin/node*
RUN rm -rf /usr/local/bin/npm*
RUN rm -rf /etc/apt/sources.list.d/nodesource.list

# Install NodeJS 20
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get update && apt-get install -y \
    nodejs \
    gcc \
    g++ \
    make

# Clear cache
RUN apt-get clean  \&& rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl pdo
RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
RUN docker-php-ext-install gd

# # Install Extension SourceGuardian
# COPY ./docker/ixed.7.2.lin /usr/local/lib/php/extensions/no-debug-non-zts-20170718/ixed.7.2.lin
# RUN ls -al /usr/local/lib/php/extensions/no-debug-non-zts-20170718/ixed.7.2.lin
# RUN echo "extension=ixed.7.2.lin" >> /usr/local/etc/php/php.ini

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
#COPY . /app

# Copy existing application directory permissions
#COPY --chown=www:www . /app

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000 8081
CMD ["php-fpm"]
