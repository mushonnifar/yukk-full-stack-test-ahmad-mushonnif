deploy:
	@echo "Copy .env.example to .env"
	cp .env.example .env
	@echo "Install dependencies"
	composer install
	@echo "Generate key"
	php artisan key:generate
	@echo "Migrate"
	php artisan migrate
	@echo "Storage link"
	php artisan storage:link

