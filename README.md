
# Quick Start

## Installation (Docker)

run container

```bash
docker compose up -d
```

run command in `app-yukk` running container
```bash
docker exec -it app-yukk sh
```
exec deployment script
```bash
make deploy
```