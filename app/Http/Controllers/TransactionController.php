<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionRequest;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = Transaction::where('created_by', Auth::user()->id);
            return DataTables::of($model)
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true); 
        }
        $user = User::find(Auth::user()->id);
        $data = [
            'user' => $user
        ];
        return view('pages.transaction.index', $data);
    }

    public function create()
    {
        $user = User::find(Auth::user()->id);
        $data = [
            'user' => $user
        ];
        return view('pages.transaction.create', $data);
    }

    public function store(TransactionRequest $request)
    {
        $payload = $request->validationData();
        $payload['code'] = 'TRX' . time();
        DB::beginTransaction();
        try {
            $model = Transaction::create($payload);

            if ($request->hasFile('evidence')) {
                $fileName = 'evidence_' . $model->id . '.' . $request->file('evidence')->extension();
                Storage::delete($fileName);
                Storage::putFileAs(
                    'evidence',
                    $request->file('evidence'),
                    $fileName
                );

                $model->evidence = 'evidence/' . $fileName;
                $model->save();
            }

            if ($payload['type'] == 'top_up') {
                $user = User::find(Auth::user()->id);
                $user->balance += $payload['amount'];
                $user->save();
            } elseif ($payload['type'] == 'transaksi') {
                $user = User::find(Auth::user()->id);
                if ($user->balance < $payload['amount']) {
                    throw new \Exception('Insufficient balance');
                }
                $user->balance -= $payload['amount'];
                $user->save();
            }
        } catch (\Exception $e) {
            DB::rollback();

            return Redirect::back()->with('error', $e->getMessage());
        }

        DB::commit();
        return Redirect::back()->with('success', 'Transaction created successfully');
    }
}
