<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\File;

class TransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'type' => ['required', 'string', 'in:top_up,transaksi'],
            'amount' => ['required', 'numeric', 'gt:0'],
            'note' => ['required', 'string'],
            'evidence' => [
                'required_if:type,top_up',
                File::image()->max('2mb'),
            ]
        ];
    }
}
