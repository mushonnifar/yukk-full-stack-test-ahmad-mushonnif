<x-default-layout>
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10">
            <!--begin::Toolbar container-->
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <!--begin::Toolbar wrapper-->
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <!--begin::Page title-->
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex flex-column justify-content-center text-dark fw-bold fs-3 m-0">Create Transaction</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--begin::Layout-->
                <div class="d-flex flex-column flex-lg-row">
                    <!--begin::Content-->
                    <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                        <!--begin::Card-->
                        <div class="card">
                            <!--begin::Card body-->
                            <div class="card-body p-12">
                                <!--begin::Form-->
                                @if (session()->has('success'))
                                <!--begin::Alert-->
                                <div class="alert alert-dismissible bg-light-primary d-flex flex-column flex-sm-row p-5 mb-10">
                                    <!--begin::Icon-->
                                    <i class="ki-duotone ki-notification-bing fs-2hx text-primary me-4 mb-5 mb-sm-0"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                                    <!--end::Icon-->

                                    <!--begin::Wrapper-->
                                    <div class="d-flex flex-column pe-0 pe-sm-10">
                                        <!--begin::Title-->
                                        <h4 class="fw-semibold text-primary">Success</h4>
                                        <!--end::Title-->

                                        <!--begin::Content-->
                                        <span class="text-primary">{{ session()->get('success') }}</span>
                                        <!--end::Content-->
                                    </div>
                                    <!--end::Wrapper-->

                                    <!--begin::Close-->
                                    <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                                        <i class="ki-duotone ki-cross fs-1 text-primary"><span class="path1"></span><span class="path2"></span></i>
                                    </button>
                                    <!--end::Close-->
                                </div>
                                <!--end::Alert-->
                                @elseif (session()->has('error'))
                                <!--begin::Alert-->
                                <div class="alert alert-dismissible bg-light-danger d-flex flex-column flex-sm-row p-5 mb-10">
                                    <!--begin::Icon-->
                                    <i class="ki-duotone ki-notification-bing fs-2hx text-danger me-4 mb-5 mb-sm-0"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                                    <!--end::Icon-->

                                    <!--begin::Wrapper-->
                                    <div class="d-flex flex-column pe-0 pe-sm-10">
                                        <!--begin::Title-->
                                        <h4 class="fw-semibold text-danger">Fail</h4>
                                        <!--end::Title-->

                                        <!--begin::Content-->
                                        <span class="text-danger">{{ session()->get('error') }}</span>
                                        <!--end::Content-->
                                    </div>
                                    <!--end::Wrapper-->

                                    <!--begin::Close-->
                                    <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                                        <i class="ki-duotone ki-cross fs-1 text-danger"><span class="path1"></span><span class="path2"></span></i>
                                    </button>
                                    <!--end::Close-->
                                </div>
                                <!--end::Alert-->
                                @endif
                                <form action="{{ route('transaction.store') }}" method="POST" enctype="multipart/form-data" id="form-create">
                                    @csrf
                                    <!--begin::Wrapper-->
                                    <div class="mb-0">
                                        <!--begin::Row-->
                                        <div class="row gx-10 mb-5">
                                            <!--begin::Col-->
                                            <div class="col-lg-12">
                                                <div class="form-group mb-5">
                                                    <label class="form-label fw-bold fs-6 text-gray-700">Type</label>
                                                    <select name="type" id="trx-type" data-control="select2" data-placeholder="Select type" class="form-select {{ $errors->get('type') ? 'is-invalid' : '' }}">
                                                        <option value=""></option>
                                                        <option value="top_up" {{ old('type') == 'top_up' ? 'selected' : '' }}>
                                                            Topup
                                                        </option>
                                                        <option value="transaksi" {{ old('type') == 'transaksi' ? 'selected' : '' }}>
                                                            Transaction
                                                        </option>
                                                    </select>
                                                    @if ($errors->get('type'))
                                                    <div class="invalid-feedback">{{ $errors->first('type') }}</div>
                                                    @endif
                                                </div>
                                                <div class="form-group mb-5">
                                                    <label class="form-label fs-6 fw-bold text-gray-700">Amount</label>
                                                    <input type="number" class="form-control text-end {{ $errors->get('amount') ? 'is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" />
                                                    @if ($errors->get('amount'))
                                                    <div class="invalid-feedback">{{ $errors->first('amount') }}</div>
                                                    @endif
                                                </div>
                                                <div class="form-group mb-5">
                                                    <label class="form-label fs-6 fw-bold text-gray-700">Note</label>
                                                    <textarea name="note" class="form-control {{ $errors->get('note') ? 'is-invalid' : '' }}" rows="3">{{ old('note') }}</textarea>
                                                    @if ($errors->get('note'))
                                                    <div class="invalid-feedback">{{ $errors->first('note') }}</div>
                                                    @endif
                                                </div>
                                                <div class="form-group mb-5" id="evidence-group">
                                                    <label class="form-label fs-6 fw-bold text-gray-700">Payment Slip</label>
                                                    <input type="file" name="evidence" accept="image/*" class="form-control {{ $errors->get('evidence') ? 'is-invalid' : '' }}">
                                                    @if ($errors->get('evidence'))
                                                    <div class="invalid-feedback">{{ $errors->first('evidence') }}</div>
                                                    @endif
                                                </div>
                                                <button type="submit" class="btn btn-primary w-100" id="btn-submit">
                                                    <i class="ki-outline ki-triangle fs-3"></i>Create Transaction</button>
                                            </div>
                                            <!--end::Col-->
                                        </div>
                                        <!--end::Row-->
                                    </div>
                                    <!--end::Wrapper-->
                                </form>
                                <!--end::Form-->
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Content-->
                    <!--begin::Sidebar-->
                    <div class="flex-lg-auto min-w-lg-400px">
                        <!--begin::Card-->
                        <div class="card" data-kt-sticky="true" data-kt-sticky-name="invoice" data-kt-sticky-offset="{default: false, lg: '200px'}" data-kt-sticky-width="{lg: '250px', lg: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="150px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
                            <!--begin::Card body-->
                            <div class="card-body p-10">
                                <div class="mb-4 px-9">
                                    <!--begin::Info-->
                                    <div class="d-flex align-items-center mb-2">
                                        <!--begin::Currency-->
                                        <span class="fs-4 fw-semibold text-gray-400 align-self-start me-1>">Rp</span>
                                        <!--end::Currency-->
                                        <!--begin::Value-->
                                        <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1">{{ number_format($user->balance, 2, ',', '.') }}</span>
                                        <!--end::Value-->
                                    </div>
                                    <!--end::Info-->
                                    <!--begin::Description-->
                                    <span class="fs-6 fw-semibold text-gray-400">Balance</span>
                                    <!--end::Description-->
                                </div>
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Sidebar-->
                </div>
                <!--end::Layout-->
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
    @push('scripts')
    <script src="{{ asset('assets/js/custom/apps/invoices/create.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#evidence-group').hide();
        });
        $('#trx-type').on('change', function() {
            if ($(this).val() == 'top_up') {
                $('#evidence-group').show();
            } else {
                $('#evidence-group').hide();
            }
        });
        $('#form-create').on('submit', function(e) {
            $('#btn-submit').prop('disabled', true);
        });
    </script>
    @endpush
</x-default-layout>